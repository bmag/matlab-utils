function [ result ] = eexp( x )
%eexp Natural exponent of X with error margins
%   X should be a table, where X.val and X.err are vectors of the same
%   size.
%   The result is a table with resule.val = exp(X.val), and result.err = a
%   vector of error margin for each element in result.val.
%
%   Example:
%   >>> x = table(1:4, zeros(1,4)+0.2, 'VariableNames', {'val', 'err'});
%   >>> x.val
%   0 1 2 3
%   >>> r = eexp(x);
%   >>> r.val
%   1.0000 2.7183 7.3891 20.0855

val = exp(x.val);
err = (x.err) .* (val);
result = table(val, err);

end

