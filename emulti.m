function [ result ] = emulti( x, y )
%emulti Multiply X*Y with error margins
%   X and Y should be two tables, where X.val, X.err, Y.val and Y.err are
%   all vectors of the same size. The result is a table with
%   resule.val = X.val .* Y.val, and result.err = a vector of error margin
%   for each element in result.val.
%   >>> x = table(1:4, zeros(1,4)+0.2, 'VariableNames', {'val', 'err'});
%   >>> y = table(2:5, zeros(1,4)+0.2, 'VariableNames', {'val', 'err'});
%   >>> r = emulti(x, y);

val = x.val .* y.val;
err = sqrt((y.val .* x.err).^2 + (x.val .* y.err).^2);
result = table(val, err);

end

