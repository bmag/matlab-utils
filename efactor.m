function [ result ] = efactor( a, x )
%efactor Multiply A*X with error margins
%   X should be a table, where X.val and X.err are vectors of the same
%   size. A should be a scalar.
%   The result is a table with resule.val = A * X.val, and result.err = a
%   vector of error margin for each element in result.val.
%   >>> x = table(1:4, zeros(1,4)+0.2, 'VariableNames', {'val', 'err'});
%   >>> r = efactor(2, x);

val = a * x.val;
err = a * x.err;
result = table(val, err);

end

