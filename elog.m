function [ result ] = elog( x )
%elog Natural log of X with error margins
%   X should be a table, where X.val and X.err are vectors of the same
%   size.
%   The result is a table with resule.val = log(X.val), and result.err = a
%   vector of error margin for each element in result.val.
%
%   Example:
%   >>> x = table(1:4, zeros(1,4)+0.2, 'VariableNames', {'val', 'err'});
%   >>> r = elog(x);

val = log(x.val);
err = (x.err) ./ (x.val);
result = table(val, err);

end

